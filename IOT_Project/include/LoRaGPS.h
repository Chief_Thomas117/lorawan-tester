/*--------------------------------------------------------------------
  This file is part of the LoraWAN tester.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Thomas Randwijk Email: tgf.randwijk@student.han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/
/**
 * @file LoRaGPS.h
 * @author Vincent de Man (V.deMan@student.han.nl)
 * @brief Gps handler class for air530Z
 * @version 0.1
 * @date 2022-02-15
 * @details Gps handler class for air530Z
 * @copyright Copyright (c) 2022
 *
 */

#ifndef LORAGPS
#define LORAGPS

#include "GPS_Air530Z.h"

class LoRaGPS
{
public:
    /**
     * @brief Construct a new LoRaGPS object
     * @details Construct new object and calls begin function from GPS.begin
     */
    LoRaGPS();

    /**
    * @brief initializes the gps for use
    * 
    */
    void init();

    /**
     * @brief update the internal gps data
    * 
    */
    void GPSupdate();

    /**
    * @brief Keeps updating the gps until timeot of param milliseconds
    * 
    * @param Miliseconds The time to run the gps
    */
    void GPSrun(int Miliseconds = 30000);

    /**
    * @brief Gets the year data from the gps
    * 
    * @return uint16_t The current year
    */
    uint16_t getYear();

    /**
    * @brief Get the day data from the gps
    * 
    * @return uint8_t The current day
    */
    uint8_t getDay();

    /**
    * @brief Get the month data from the gps
    * 
    * @return uint8_t The current month
    */
    uint8_t getMonth();

    /**
    * @brief Get the hour data from the gps
    * 
    * @return uint8_t The current hour 
    */
    uint8_t getHour();

    /**
    * @brief Get the minute data from the gps
    * 
    * @return uint8_t the current minute we are on
    */
    uint8_t getMinute();

    /**
    * @brief Get the seconds data from the gps
    * 
    * @return uint8_t the current second we are on
    */
    uint8_t getSecond();

    /**
    * @brief Get the amount of connected sattelites from the gps
    * 
    * @return uint8_t The amount of sattelites
    */
    uint8_t getSats();

    /**
     * @brief Gets the HDOP from gps
     * 
     * @return double the HDOP
     */
    double getHDOP();

    /**
    * @brief Gets the altitude from the gps
    * 
    * @return double the current altitude
    */
    double getAlt();

    /**
    * @brief Get the latitude form gps
    * 
     * @return double the Latitude
    */
    double getLat();

    /**
    * @brief returns the longitude data
    * 
    * @return double the longitude
    */
    double getLng();

private:

    Air530ZClass GPS; ///< GPS object to interact with the gps module
};

#endif