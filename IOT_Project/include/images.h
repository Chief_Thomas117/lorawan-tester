/*--------------------------------------------------------------------
  This file is part of the LoraWAN tester.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Thomas Randwijk Email: tgf.randwijk@student.han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/
/**
 * @file images.h
 * @author Thijs van Elsacker
 * @brief This file contans some nice images to use in the project.
 * @version 0.1
 * @date 2022-04-03
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef IMAGES_H
#define IMAGES_H
#include <stdint.h>

#define Hexagon_Logo_width 118
#define Hexagon_Logo_height 63
/**
 * @brief The display logo
 * 
 */
const uint8_t Hexagon_Logo_bits[] PROGMEM = {
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
  0xFF, 0xFF, 0x3F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0x7F, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x3F, 
  0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x3E, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x3C, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3C, 0x07, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0xF0, 0xF3, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3E, 0x00, 0x1F, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x07, 0x0C, 0xF8, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0x00, 0x7C, 
  0xC0, 0xFF, 0x80, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 
  0x00, 0x80, 0x0F, 0xF8, 0xFF, 0x07, 0x7C, 0x00, 0x00, 0x00, 0x00, 0x38, 
  0x07, 0x00, 0x00, 0x00, 0xE0, 0x00, 0xFF, 0xFF, 0x3F, 0xC0, 0x00, 0x00, 
  0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0x60, 0xE0, 0xFF, 0xFF, 0xFF, 
  0x81, 0x01, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0x60, 0xF8, 
  0xFF, 0xFF, 0xFF, 0x87, 0x01, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 
  0x00, 0x60, 0xF8, 0xFF, 0xFF, 0xFF, 0x87, 0x01, 0x00, 0x00, 0x00, 0x38, 
  0x07, 0x00, 0x00, 0x00, 0x60, 0xF8, 0xFF, 0xFF, 0xFF, 0x87, 0x01, 0x00, 
  0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0x60, 0xF8, 0xFF, 0xFF, 0xFF, 
  0x87, 0x01, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0x60, 0xF8, 
  0xFF, 0xFF, 0xFF, 0x87, 0x01, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 
  0x00, 0x60, 0xF8, 0xFF, 0xFF, 0xFF, 0x87, 0x01, 0x00, 0x00, 0x00, 0x38, 
  0x07, 0x00, 0x00, 0x00, 0x60, 0xF8, 0xFF, 0xFF, 0xFF, 0x87, 0x01, 0x00, 
  0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0x60, 0xF8, 0xFF, 0xFF, 0xFF, 
  0x87, 0x01, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0x60, 0xF8, 
  0xFF, 0xFF, 0xFF, 0x87, 0x01, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 
  0x00, 0x3E, 0xE0, 0xFF, 0xFF, 0xFF, 0x01, 0x1F, 0x00, 0x00, 0x00, 0x38, 
  0x07, 0x00, 0x00, 0xC0, 0x07, 0x80, 0xFF, 0xFF, 0x7F, 0x00, 0xF8, 0x00, 
  0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x7C, 0x80, 0x07, 0xF8, 0xFF, 0x07, 
  0x78, 0xC0, 0x07, 0x00, 0x00, 0x38, 0x07, 0x00, 0x80, 0x0F, 0xF0, 0x3F, 
  0x80, 0x7F, 0x80, 0xFF, 0x07, 0x7C, 0x00, 0x00, 0x38, 0x07, 0x00, 0xF0, 
  0x01, 0xFE, 0xFF, 0x03, 0x0C, 0xF0, 0xFF, 0x3F, 0xF0, 0x03, 0x00, 0x38, 
  0x07, 0x00, 0x38, 0xE0, 0xFF, 0xFF, 0x1F, 0x00, 0xFE, 0xFF, 0xFF, 0x01, 
  0x07, 0x00, 0x38, 0x07, 0x00, 0x18, 0xFC, 0xFF, 0xFF, 0xFF, 0x80, 0xFF, 
  0xFF, 0xFF, 0x0F, 0x06, 0x00, 0x38, 0x07, 0x00, 0x18, 0xFE, 0xFF, 0xFF, 
  0xFF, 0xE1, 0xFF, 0xFF, 0xFF, 0x1F, 0x06, 0x00, 0x38, 0x07, 0x00, 0x18, 
  0xFE, 0xFF, 0xFF, 0xFF, 0xE1, 0xFF, 0xFF, 0xFF, 0x1F, 0x06, 0x00, 0x38, 
  0x07, 0x00, 0x18, 0xFE, 0xFF, 0xFF, 0xFF, 0xE1, 0xFF, 0xFF, 0xFF, 0x1F, 
  0x06, 0x00, 0x38, 0x07, 0x00, 0x18, 0xFE, 0xFF, 0xFF, 0xFF, 0xE1, 0xFF, 
  0xFF, 0xFF, 0x1F, 0x06, 0x00, 0x38, 0x07, 0x00, 0x18, 0xFE, 0xFF, 0xFF, 
  0xFF, 0xE1, 0xFF, 0xFF, 0xFF, 0x1F, 0x06, 0x00, 0x38, 0x07, 0x00, 0x18, 
  0xFE, 0xFF, 0xFF, 0xFF, 0xE1, 0xFF, 0xFF, 0xFF, 0x1F, 0x06, 0x00, 0x38, 
  0x07, 0x00, 0x18, 0xFE, 0xFF, 0xFF, 0xFF, 0xE1, 0xFF, 0xFF, 0xFF, 0x1F, 
  0x06, 0x00, 0x38, 0x07, 0x00, 0x18, 0xFE, 0xFF, 0xFF, 0xFF, 0xE1, 0xFF, 
  0xFF, 0xFF, 0x1F, 0x06, 0x00, 0x38, 0x07, 0x00, 0x18, 0xFE, 0xFF, 0xFF, 
  0xFF, 0xE1, 0xFF, 0xFF, 0xFF, 0x1F, 0x06, 0x00, 0x38, 0x07, 0x00, 0x38, 
  0xF8, 0xFF, 0xFF, 0x7F, 0xC0, 0xFF, 0xFF, 0xFF, 0x0F, 0x07, 0x00, 0x38, 
  0x07, 0x00, 0x78, 0x80, 0xFF, 0xFF, 0x0F, 0x00, 0xFE, 0xFF, 0xFF, 0x80, 
  0x07, 0x00, 0x38, 0x07, 0x00, 0xE0, 0x03, 0xFE, 0xFF, 0x03, 0x1E, 0xF0, 
  0xFF, 0x3F, 0xF0, 0x01, 0x00, 0x38, 0x07, 0x00, 0x00, 0x1E, 0xF0, 0x1F, 
  0xF0, 0xF3, 0x03, 0xFF, 0x03, 0x3E, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 
  0xF0, 0x00, 0x07, 0x3C, 0x00, 0x0F, 0x30, 0xC0, 0x01, 0x00, 0x00, 0x38, 
  0x07, 0x00, 0x00, 0x80, 0x0F, 0xC0, 0x07, 0x00, 0xF8, 0x00, 0x7C, 0x00, 
  0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0xFC, 0xF8, 0x00, 0x00, 0xC0, 
  0xCF, 0x07, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0x80, 0x0F, 
  0x00, 0x00, 0x00, 0xFC, 0x00, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0x00, 0xE0, 
  0xFB, 0x9C, 0x88, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 
  0x00, 0x00, 0x80, 0x08, 0xA2, 0x8D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 
  0x07, 0x00, 0x00, 0x00, 0x00, 0x80, 0x08, 0xA2, 0x8A, 0x07, 0x00, 0x00, 
  0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0x00, 0x80, 0x78, 0xBE, 0x0A, 
  0x08, 0x00, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 0x00, 0x00, 0x80, 
  0x08, 0xA2, 0x08, 0x08, 0x00, 0x00, 0x00, 0x00, 0x38, 0x07, 0x00, 0x00, 
  0x00, 0x00, 0x80, 0x08, 0xA2, 0x88, 0x08, 0x00, 0x00, 0x00, 0x00, 0x38, 
  0x0F, 0x00, 0x00, 0x00, 0x00, 0x80, 0xF8, 0xA2, 0x08, 0x07, 0x00, 0x00, 
  0x00, 0x00, 0x3C, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x3C, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3E, 0x7F, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x3F, 
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
  0xFF, 0xFF, 0x3F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, };
  
  #endif