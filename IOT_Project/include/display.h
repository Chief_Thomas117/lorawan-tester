/*--------------------------------------------------------------------
  This file is part of the LoraWAN tester.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Thomas Randwijk Email: tgf.randwijk@student.han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/
/**
 * @file display.h
 * @author Thijs van Elsacker
 * @brief Main class for the UserInterfase
 * @version 2.2
 * @date 2022-03-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef display_h
#define display_h

#include <Arduino.h>
#include <Wire.h>               
#include "HT_SSD1306Wire.h"
#include "images.h"

//-------------------------Button Functions----------------------
    /**
     * @brief Updates screen to the corresponding next screen when the upper button is pressed
     * 
     * @return ** void 
     */
    void interruptHandlerUp();

    /**
     * @brief Updates screen to the corresponding next screen when the lower button is pressed
     * 
     * @return ** void 
     */
    void interruptHandlerDown();

    /**
     * @brief Updates screen to the corresponding next screen when the enter button is pressed
     * 
     * @return ** void 
     */
    void interruptHandlerEnter();

    /**
     * @brief Updates screen to the corresponding next screen when the cancel button is pressed.
     * 
     * @return ** void 
     */
    void interruptHandlerCancel();

    /**
     * @brief Initializing buttons and interrupts
     * 
     * @return ** void
     */
    void initButtons();

    extern const int buttonPinUp;                                   ///< Pin number for the "up" option button
    extern const int buttonPinDown;                                 ///< Pin number for the "down" option button
    extern const int buttonPinEnter;                                ///< Pin number for the "enter" option button
    extern const int buttonPinCancel;                               ///< Pin number for the "cancel" option button
//-------------------------Button Functions----------------------

//-------------------------SETUP init----------------------------
    extern SSD1306Wire display1;                                    ///< addr , freq , SDA, SCL, resolution , rst

    extern const int displayFirstLine;                              ///< Number for the first line of display
    extern const int displaySecondLine;                             ///< Number for the second line of display
    extern const int displayThirdLine;                              ///< Number for the third line of display
    extern const int displayFourthLine;                             ///< Number for the fourth line of display
    extern const int displayFifthLine;                              ///< Number for the fifth line of display
    extern const int displaySixthLine;                              ///< Number for the sixth line of display

    extern volatile int IntervalTimer;                              ///< Timer int for selecting interval timing between measuring points.
    extern const int offset;                                        ///< Offset for displaying strings on the display.
    extern const int displayWidth;                                  ///< Max width of display for displaying strings on the display.
 
    extern int ScreenStatus;                                        ///< Keeps track of the current screen status
    extern int PreviousScreenStatus;                                ///< Keeps track of the previous screen status

    /**
     * @brief ENUM contains all screen states
     * 
     */
    typedef enum ScreenState{
            E_Logo = 0,
            E_MainMenuHighlightClicksend = 1,
            E_MainMenuHighlightInterval = 2,
            E_MainMenuHighlightOptions = 3,
            E_MainMenuHighlightExit = 4,
    
            E_ClickSendScreenIDLE = 10,
                E_ClickSendScreenGPS = 100,
                E_ClickSendScreenSENDING_MESSAGE = 101,
                E_ClickSendScreenSUCCESFULL = 102,
                E_ClickSendScreenFAILED = 103,
    
            E_DistanceIntervalScreen = 20,
    
            E_optionscreenHighlightBrightness = 30,
                E_BrightnessHighlightLow = 300,
                E_BrightnessHighlightMed = 301,
                E_BrightnessHighlightHigh = 302,
                    E_BrightnessSelectLow = 3000,
                    E_BrightnessSelectMed = 3010,
                    E_BrightnessSelectHigh = 3020,
    
            E_optionscreenHighlightCredits = 31,
                E_CreditScreen = 310,
            E_ExitScreen = 40
    };
//-------------------------SETUP init---------------------------- 
class UserInterfase
{ 
public:
    /**
    * @brief Initializing UserInterfase
    * 
    * @return ** void 
    */
    void Init();
    /**
     * @brief Constructor for the UserInterfase
     * 
     */
    UserInterfase();
    /**
     * @brief Clears screen and updates screen
     * 
     * @return which screen its currently on in int value
     */
    int RefreshScreen();
    /**
     * @brief Sets screenstatus to the correct screen
     * 
     * @return The value of screenstatus in int
     */
    int ShowScreen();
     
private:
    /**
     * @brief Displays the startup screen
     * 
     * @return ** void 
     */
    void StartUpScreen();
    /**
     * @brief Displays the Main Menu screen
     * 
     * @param mainMenuselection Int to control which options needs to be selected in the Main Menu screen
     * @return ** void 
     */
    void MainMenuScreen(int mainMenuselection);
    /**
     * @brief Displays the ClickSend screen
     * 
     * @param clickSendSelection Int to set the Click Send status
     * @return ** void 
     */
    void ClickSendScreen(int clickSendSelection);
    /**
     * @brief Displays the Distance Interval screen
     * 
     * @return ** void 
     */
    void DistanceIntervalScreen(); 
    /**
     * @brief Displays all the options for the Brightness
     * 
     * @param BrightnessSelection Int to control which options needs to be selected in the Brightness screen
     * @return ** void 
     */
    void BrightnessScreen(int BrightnessSelection); 
    /**
     * @brief Displays Options screen
     * 
     * @param optionsSelection Int to control which options needs to be selected in the Options screen
     * @return ** void 
     */
    void OptionsScreen(int optionsSelection); 
    /**
     * @brief Displays CreditScreen
     * 
     * @return ** void 
     */
    void CreditScreen(); 
    /**
     * @brief Displays Exitscreen and after turns off screen
     * 
     * @return ** void 
     */
    void ExitScreen();
    /**
     * @brief Initializing display
     * 
     * @return ** void 
     */
    void InitDisplay();
    /**
     * @brief Adds [Enter] to the bottom left of the screen and [Cancel] to the bottem right of the screen
     * 
     * @return ** void 
     */
    void DisplayScreen();
    /**
     * @brief Set the Brightness object
     * 
     * @param brightness Int between 0 - 127 to determine the brightness
     * @return ** void 
     */
    void SetBrightness(int brightness);
    /**
     * @brief Go back to the previous screen
     * 
     * @return ** void 
     */
    void GoBackMenu();
    /**
     * @brief ENUM with all the levels of brightness
     * 
     */
    enum Brightness 
    {
        LOW_BRIGHT = 1, 
        NORMAL_BRIGHT = 127, 
        HIGH_BRIGHT = 255
    };
};

#endif