/*--------------------------------------------------------------------
  This file is part of the LoraWAN tester.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Thomas Randwijk Email: tgf.randwijk@student.han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/
/**
 * @file LoRaConnect.h
 * @author Thomas Randwijk (tgf.randwijk@student.han.nl)
 * @brief Lora Connect Library for STM heltec lorawan tester.
 * @version 1.0
 * @date 2022-03-07
 *
 * @copyright Copyright (c) 2022
 *
 *
 */

#ifndef LORACONNECT_H
#define LORACONNECT_H

#include <array>
#include "LoRaWan_APP.h"
#include "Arduino.h"
#include <stdio.h>

//-------------------------SETUP init----------------------------
// These setups are defined and initialised in the cpp file.
    #define ATSUPPORT 0;
    extern uint8_t devEui[];                ///< Device ID, provided by manufacturer
    extern uint8_t appEui[];                ///< Join EUI, for identifying in OTAA. Used to find IP adres of join server via DNS
    extern uint8_t appKey[];                ///< Application Key, encryption key

    /* ABP para*/
    extern uint8_t nwkSKey[];               ///< Network Session Key for ABP
    extern uint8_t appSKey[];               ///< Application Session Key
    extern uint32_t devAddr;                ///< Device address
    extern uint16_t userChannelsMask[6];    ///< LoraWan channelsmask, default channels 0-7
    extern LoRaMacRegion_t loraWanRegion;   ///< LoraWan region, select in arduino IDE tools
    extern DeviceClass_t  loraWanClass;     ///< LoraWan Class, Class A and Class C are supported
    extern uint32_t appTxDutyCycle;         ///< the application data transmission duty cycle.  value in [ms].
    extern bool overTheAirActivation;       ///< OTAA or ABP, OTAA is prefered
    extern uint8_t appPort;                 ///< Application port to be used by sending.
    extern bool loraWanAdr;                 ///< ADR enable
    extern bool keepNet;                    ///< set LORAWAN_Net_Reserve ON, the node could save the network info to flash, when node reset not need to join again 
    extern bool isTxConfirmed;              ///<  Indicates if the node is sending confirmed or unconfirmed messages 
    extern uint8_t confirmedNbTrials;       ///< number of trials to send. 
//-------------------------SETUP init--------------------------------

//-----------------------CLASS---------------------------------------
class LoRaConnect
{
public:
    /**
     * @brief Construct a new Lo Ra Connect object, with setup / initalization to get the LoRa stuff working
     *
     */
    LoRaConnect(int init = 0);

    /**
     * @brief Setup LoRawan MCU, initialization function to be used in setup
     *
     */
    void init();

    /**
     * @brief Insert byte to send with LoraSend Function, used internally.
     * 
     * @param byte 
     */
    void insertByte(uint8_t byte);

    /**
     * @brief Send function with automatic prepare tx frame.
     *
     * @param input char array uint8t
     */
    void loraSend(uint8_t *input, int size);

    /**
     * @brief Send function without tx frame, without uint8t frame.
     *
     * @param input uint8_t array to send
     * @param size Size of array
     */
    void loraSend();

    /**
     * @brief Function to prepare the byte array for a specific port. Use this in combination with an encoder function.
     *
     *
     * @param input Input array in bytes, standard type is uint8_t
     * @param size Size of array
     */
    static void prepareTxFrame(uint8_t *input, int size);

    /**
     * @brief Destroy the Lo Ra Connect object
     *
     */
    ~LoRaConnect();
};

#endif
