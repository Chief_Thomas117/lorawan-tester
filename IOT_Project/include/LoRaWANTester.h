/*--------------------------------------------------------------------
  This file is part of the LoraWAN tester.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Thomas Randwijk Email: tgf.randwijk@student.han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/
/**
 * @file LoRaWANTester.cpp
 * @author Vincent de Man (V.deMan@student.han.nl)
 * @brief Main class for the loraWAN tester
 * @version 0.1
 * @date 2022-02-15
 *
 * @copyright Copyright (c) 2022
 *
 * 
 */

#ifndef LORAWANTESTER
#define LORAWANTESTER

#define Button VBAT_ADC_CTL

#include "LoRaConnect.h"
#include "Arduino.h"
#include "LoRaGPS.h"
#include "display.h"
#include <math.h>
/**
 * @brief Main class for LoRaWAN tester
 * @details
 */
class LoRaWANTester
{
public:
    /**
     * @brief Construct a new Lo Ra W A N Tester object
     */
    LoRaWANTester();
    /**
     * @brief Main loop for LoraWanTester
     * 
     */
    void Loop();
    /**
     * @brief Initialisation of LoraWanTester
     * 
     */
    void Init();


private:
    /**
     * @brief disables interupts for up down and enter
     * 
     */
    void disableUpDownEnter();

    /**
     * @brief enables interupts for up down and enter
     * 
     */
    void enableUpDownEnter();
    
    /**
     * @brief Sends gps Coords
     * 
     */
    void sendGPS();

    /**
     * @brief Creates encoded message string
     * 
     * @param lat lattitude
     * @param lng longitude
     * @param alt altitude
     * @param hdop accuricy
     * @param arr array to put it in to
     */
    void encodeMessage(const double lat, const double lng, const double alt, const double hdop, uint8_t (&arr)[8]);



    
    LoRaGPS GPS; ///< GPS object to interact with the gps module
    LoRaConnect antenne; ///< LoRaConnect object to interact with the gps module
    UserInterfase UI; ///< UserInterface object to interact with the gps module
};

#endif