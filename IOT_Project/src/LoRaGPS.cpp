/*--------------------------------------------------------------------
  This file is part of the LoraWAN tester.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Thomas Randwijk Email: tgf.randwijk@student.han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/
/**
 * @file LoraGPS.cpp
 * @author Vincent de Man (V.deMan@student.han.nl)
 * @brief Gps handler class for air530Z
 * @version 0.1
 * @date 2022-02-15
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "LoRaGPS.h"


LoRaGPS::LoRaGPS()
{
}

void LoRaGPS::init()
{
    GPS.begin(115200);
}

void LoRaGPS::GPSupdate()
{
    if(GPS.available() > 0){
    GPS.encode(GPS.read());
    }
}

void LoRaGPS::GPSrun(int Miliseconds )
{
    uint32_t startTime = millis();
    while ((millis() - startTime) < Miliseconds)
    { 
        while (GPS.available() > 0)
        {
            GPS.encode(GPS.read());
        }
    }

    if (millis() > 5000 && GPS.charsProcessed() < 10)
    {
        Serial.println("No GPS detected: check wiring.");
    }
}

uint16_t LoRaGPS::getYear()
{
    return GPS.date.year();
}

uint8_t LoRaGPS::getDay()
{
    return GPS.date.day();
}

uint8_t LoRaGPS::getMonth()
{
    return GPS.date.month();
}

uint8_t LoRaGPS::getHour()
{
    return GPS.time.hour();
}

uint8_t LoRaGPS::getMinute()
{
    return GPS.time.minute();
}

uint8_t LoRaGPS::getSecond()
{
    return GPS.time.second();
}

uint8_t LoRaGPS::getSats()
{
    return GPS.satellites.value();
}

double LoRaGPS::getAlt()
{
    return GPS.altitude.kilometers();
}

double LoRaGPS::getHDOP()
{
    return GPS.hdop.hdop();
}

double LoRaGPS::getLat()
{
    return GPS.location.lat();
}

double LoRaGPS::getLng()
{
    return GPS.location.lng();
}
