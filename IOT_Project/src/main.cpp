/*--------------------------------------------------------------------
  This file is part of the LoraWAN tester.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Thomas Randwijk Email: tgf.randwijk@student.han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/
/**
 * @file main.cpp
 * @authors Vincent de Man, Ruben Pomstra, Marleen Beekman, Remco Sluijs, Thijs van Elsacker, Thomas Randwijk
 * @brief This is the main file of the project. see detailed
 * @details for more information, please read the readme of this project. 
 * Also look at our repository for the latest version of the code: https://gitlab.com/Chief_Thomas117/lorawan-tester
 * @version 3.2
 * @date 2022-04-03
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "LoRaWANTester.h"
LoRaWANTester tester;
void setup() 
{
    tester.Init(); // initialization
}

void loop()
{
    tester.Loop(); // loop function
}
