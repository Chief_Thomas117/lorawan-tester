/*--------------------------------------------------------------------
  This file is part of the LoraWAN tester.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Thomas Randwijk Email: tgf.randwijk@student.han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/
/**
 * @file display.cpp
 * @author Thijs van Elsacker
 * @brief Main class for the UserInterfase
 * @version 0.1
 * @date 2022-04-01
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "display.h"

//-------------------------SETUP init----------------------------
    SSD1306Wire display1(0x3c, 500000, SDA, SCL, GEOMETRY_128_64, GPIO10);      ///< addr , freq , SDA, SCL, resolution , rst

    const int buttonPinUp = GPIO11;                                             
    const int buttonPinDown = GPIO5; 
    const int buttonPinEnter = GPIO6;
    const int buttonPinCancel = GPIO7; 

    const int displayFirstLine = 0;
    const int displaySecondLine = 10;
    const int displayThirdLine = 20;
    const int displayFourthLine = 30;
    const int displayFifthLine = 40;
    const int displaySixthLine = 50;

    const int offset = display1.width()/2;
    const int displayWidth = display1.width();

    volatile int IntervalTimer = 90000;  

    int ScreenStatus = 0;
    int PreviousScreenStatus = 0;                
//-------------------------SETUP init----------------------------  

void interruptHandlerUp()
{
    delay(200);
    PreviousScreenStatus = ScreenStatus;
    ScreenStatus--;
}
void interruptHandlerDown()
{
    delay(200);
    PreviousScreenStatus = ScreenStatus;
    ScreenStatus++;
    
}
void interruptHandlerEnter()
{
    delay(200);
    PreviousScreenStatus = ScreenStatus;
    ScreenStatus = ScreenStatus * 10;
}
void interruptHandlerCancel()
{
    delay(200);
    PreviousScreenStatus = ScreenStatus;
    ScreenStatus = ScreenStatus / 10;
}

void initButtons()
{
    pinMode(buttonPinUp, INPUT);
    pinMode(buttonPinDown, INPUT);
    pinMode(buttonPinEnter, INPUT);
    pinMode(buttonPinCancel, INPUT);

    attachInterrupt(digitalPinToInterrupt(buttonPinUp), interruptHandlerUp, FALLING);
    attachInterrupt(digitalPinToInterrupt(buttonPinDown), interruptHandlerDown, FALLING);
    attachInterrupt(digitalPinToInterrupt(buttonPinEnter), interruptHandlerEnter, FALLING);
    attachInterrupt(digitalPinToInterrupt(buttonPinCancel), interruptHandlerCancel, FALLING);
}

void UserInterfase::Init()
{
    display1.init();

    display1.setFont(ArialMT_Plain_10);
    initButtons();
    StartUpScreen();
    delay(500);
}

UserInterfase::UserInterfase()
{
    
}

void UserInterfase::StartUpScreen()
{
    display1.clear();
    display1.drawXbm(0, 0, Hexagon_Logo_width, Hexagon_Logo_height, Hexagon_Logo_bits);
    display1.display();
    ScreenStatus++;
}

void UserInterfase::MainMenuScreen(int mainMenuselection)
{
    InitDisplay();

    if(mainMenuselection < E_MainMenuHighlightExit)
    {
        display1.drawString(offset, displaySecondLine, (E_MainMenuHighlightClicksend == mainMenuselection) ? "> Click Send <" : "Click Send");
        display1.drawString(offset, displayThirdLine, (E_MainMenuHighlightInterval == mainMenuselection) ? "> Distance Interval <"  : "Distance Interval");
        display1.drawString(offset, displayFourthLine, (E_MainMenuHighlightOptions == mainMenuselection) ? "> Options <" : "Options");
        display1.drawString(offset, displayFifthLine, "V");
    }
    else
    {
        display1.drawString(offset, displaySecondLine, (E_MainMenuHighlightExit == mainMenuselection) ? "> Exit <"  : "Exit"); 
    }
    DisplayScreen();
}

void UserInterfase::ClickSendScreen(int clickSendSelection) 
{
    InitDisplay();

    display1.drawString(offset, displayFirstLine, "Click Send Mode"); 

    switch(clickSendSelection)
    {
        case E_ClickSendScreenIDLE:
        {
            display1.drawString(offset, displayThirdLine, "Press [Enter] to start"); 
            display1.setTextAlignment(TEXT_ALIGN_LEFT);
            display1.drawString(0, displaySixthLine, "Enter");
            display1.setTextAlignment(TEXT_ALIGN_RIGHT);
            display1.drawString(displayWidth, displaySixthLine, "Cancel");
            break;
        }
        case E_ClickSendScreenGPS:
        { 
            display1.drawString(offset, displayThirdLine, "Getting GPS data..."); 
            break;
        }
        case E_ClickSendScreenSENDING_MESSAGE:
        {
            display1.drawString(offset, displayThirdLine, "Sending message..."); 
            break;
        }
        case E_ClickSendScreenSUCCESFULL:
        {        
            display1.drawString(offset, displayThirdLine, "Succesfull!"); 
            break;
        }
        case E_ClickSendScreenFAILED:
        {
            display1.drawString(offset, displayThirdLine, "Failed!"); 
            break;
        }
    }
    display1.display();
}

void UserInterfase::DistanceIntervalScreen()
{
    InitDisplay();
  
    display1.drawString(offset, displayFirstLine, "Distance Interval Active");
    display1.drawString(offset, displayThirdLine, "Mode started!");
    display1.drawString(offset, displayFourthLine, "Click [Cancel] to Exit"); 

    display1.setTextAlignment(TEXT_ALIGN_RIGHT);
    display1.drawString(displayWidth, displaySixthLine, "Cancel");

    display1.display();
}

void UserInterfase::BrightnessScreen(int BrightnessSelection)
{
    InitDisplay();

    display1.drawString(offset, displayFirstLine, "Brightness"); 
    display1.drawString(offset, displayThirdLine, (E_BrightnessHighlightLow == BrightnessSelection) ? "> Low brightness <" : "Low brightness"); 
    display1.drawString(offset, displayFourthLine, (E_BrightnessHighlightMed == BrightnessSelection) ? "> Normal brightness <" : "Normal brightness");
    display1.drawString(offset, displayFifthLine, (E_BrightnessHighlightHigh == BrightnessSelection) ? "> High brightness <" : "High brightness");

    DisplayScreen();
}

void UserInterfase::OptionsScreen(int optionsSelection)
{
    InitDisplay();

    display1.drawString(offset, displayFirstLine, "Options"); 
    display1.drawString(offset, displayThirdLine, (E_optionscreenHighlightBrightness == optionsSelection) ? "> Brightness <" : "Brightness");
    display1.drawString(offset, displayFourthLine, (E_optionscreenHighlightCredits == optionsSelection) ? "> Credits <" : "Credits");

    DisplayScreen();
}

void UserInterfase::CreditScreen()
{
    InitDisplay();

    display1.drawString(offset, displayFirstLine, "-=Credits=-"); 
    display1.drawString(offset, displayThirdLine, "GPS Location:");
    display1.drawString(offset, displayFourthLine, "Vincent de Man");
    display1.drawString(offset, displayFifthLine, "&");
    display1.drawString(offset, displaySixthLine, "Remco Sluijs");
    display1.display();
    delay(2000);
    display1.clear();

    display1.drawString(offset, displayFirstLine, "-=Credits=-"); 
    display1.drawString(offset, displayThirdLine, "LoRaWAN connection:");
    display1.drawString(offset, displayFourthLine, "Thomas Randwijk");
    display1.drawString(offset, displayFifthLine, "&");
    display1.drawString(offset, displaySixthLine, "Marleen Beekman");
    display1.display();
    delay(2000);
    display1.clear();

    display1.drawString(offset, displayFirstLine, "-=Credits=-"); 
    display1.drawString(offset, displayThirdLine, "Gui & Hardware:");
    display1.drawString(offset, displayFourthLine, "Ruben Pomstra");
    display1.drawString(offset, displayFifthLine, "&");
    display1.drawString(offset, displaySixthLine, "Thijs van Elsacker");
    display1.display();
    delay(2000);
    display1.clear();
}
    
void UserInterfase::ExitScreen()
{
    InitDisplay();

    display1.drawString(offset, displayThirdLine, "Shutting Down."); 
    display1.display();
    delay(500);

    display1.clear();
    display1.drawString(offset, displayThirdLine, "Shutting Down.."); 
    display1.display();
    delay(500);

    display1.clear();
    display1.drawString(offset, displayThirdLine, "Shutting Down...");
    display1.display();
    delay(500);

    display1.clear();
    display1.display();
    display1.displayOff();
    while (1)
    {
        ;
    }
    
}

void UserInterfase::InitDisplay() 
{
    display1.clear();
    display1.setTextAlignment(TEXT_ALIGN_CENTER);
}

void UserInterfase::DisplayScreen() 
{
    display1.setTextAlignment(TEXT_ALIGN_LEFT);
    display1.drawString(0, displaySixthLine, "Enter");
    display1.setTextAlignment(TEXT_ALIGN_RIGHT);
    display1.drawString(displayWidth, displaySixthLine, "Cancel");

    display1.display();
}

int UserInterfase::RefreshScreen()
{
    display1.displayOn();
    display1.clear();
    int returnValue = ShowScreen();
    display1.display();
    
    return returnValue;
}

int UserInterfase::ShowScreen()
{
    switch(ScreenStatus)
    {
        case E_MainMenuHighlightClicksend: 
        case E_MainMenuHighlightInterval: 
        case E_MainMenuHighlightOptions: 
        case E_MainMenuHighlightExit:
            MainMenuScreen(ScreenStatus);
            break;
        case E_ClickSendScreenIDLE:
        case E_ClickSendScreenGPS:
        case E_ClickSendScreenSENDING_MESSAGE:
        case E_ClickSendScreenSUCCESFULL:
        case E_ClickSendScreenFAILED:
            ClickSendScreen(ScreenStatus);
            break;
        case E_DistanceIntervalScreen:
            DistanceIntervalScreen();
            break;
        case E_optionscreenHighlightBrightness: 
        case E_optionscreenHighlightCredits:
            OptionsScreen(ScreenStatus); 
            break;
        case E_BrightnessHighlightLow: 
        case E_BrightnessHighlightMed: 
        case E_BrightnessHighlightHigh:
            BrightnessScreen(ScreenStatus);
            break;
        case E_CreditScreen:
            noInterrupts();
            CreditScreen();
            ScreenStatus = E_optionscreenHighlightCredits;
            interrupts();
            break;
        case E_ExitScreen:
            ExitScreen();
            break;
        case E_BrightnessSelectLow: 
        case E_BrightnessSelectMed: 
        case E_BrightnessSelectHigh:
            SetBrightness(ScreenStatus);
            GoBackMenu();
            break;
        default:
            ScreenStatus = PreviousScreenStatus;
            break;
    }
    return ScreenStatus;
}

void UserInterfase::SetBrightness(int brightness)
{
    if(brightness == E_BrightnessSelectLow) display1.setBrightness(LOW_BRIGHT);
    if(brightness == E_BrightnessSelectMed) display1.setBrightness(NORMAL_BRIGHT);
    if(brightness == E_BrightnessSelectHigh) display1.setBrightness(HIGH_BRIGHT);
}

void UserInterfase::GoBackMenu()
{
    ScreenStatus = ScreenStatus / 10;
}
