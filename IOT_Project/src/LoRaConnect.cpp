/*--------------------------------------------------------------------
  This file is part of the LoraWAN tester.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Thomas Randwijk Email: tgf.randwijk@student.han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/
/**
 * @file LoRaConnect.cpp
 * @author Thomas Randwijk
 * @brief Lora connection handler.
 * @version 2.0
 * @date 2022-04-03
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "LoRaConnect.h"

//-------------------------SETUP----------------------------
#define ATSUPPORT 0;
uint8_t devEui[] = { 0x60, 0x81, 0xF9, 0x0B, 0x96, 0x2E, 0xD3, 0x16 }; //Old Keys! Change them before using!
uint8_t appEui[] = { 0x60, 0x81, 0xF9, 0x86, 0x57, 0x72, 0xB4, 0xFA };
uint8_t appKey[] = { 0x0A, 0x8E, 0x62, 0x5A, 0x4F, 0xDB, 0xED, 0x48, 0x7E, 0xB7, 0x29, 0xC3, 0x4C, 0x3D, 0x5A, 0x0E };

/* ABP para*/
uint8_t nwkSKey[] = {0x15, 0xb1, 0xd0, 0xef, 0xa4, 0x63, 0xdf, 0xbe, 0x3d, 0x11, 0x18, 0x1e, 0x1e, 0xc7, 0xda, 0x85};
uint8_t appSKey[] = {0xd7, 0x2c, 0x78, 0x75, 0x8c, 0xdc, 0xca, 0xbf, 0x55, 0xee, 0x4a, 0x77, 0x8d, 0x16, 0xef, 0x67};
uint32_t devAddr = (uint32_t)0x007e6ae1;
uint16_t userChannelsMask[6] = {0x00FF, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000};
LoRaMacRegion_t loraWanRegion = ACTIVE_REGION;																	 
DeviceClass_t loraWanClass = LORAWAN_CLASS;																			
uint32_t appTxDutyCycle = 1;																								
bool overTheAirActivation = LORAWAN_NETMODE;																		
uint8_t appPort = 8;																														
bool loraWanAdr = LORAWAN_ADR;																									
bool keepNet = LORAWAN_NET_RESERVE;																							
bool isTxConfirmed = LORAWAN_UPLINKMODE;																				
uint8_t confirmedNbTrials = 4;																							
//-------------------------SETUP----------------------------

LoRaConnect::LoRaConnect(int init)
{
}

void LoRaConnect::init()
{
	boardInitMcu();
	Serial.begin(115200);

#if (AT_SUPPORT)
	enableAt();
#endif
	deviceState = DEVICE_STATE_INIT;
	LoRaWAN.ifskipjoin();
}

void LoRaConnect::prepareTxFrame(uint8_t *input, int size)
{
	appDataSize = size;
	for (auto i = 0; i < size; i++)
	{
		appData[i] = input[i];
	}
	appData[size] = NULL;
}

void LoRaConnect::loraSend(uint8_t *input, int size)
{
	signed int resetflag = 1;
	while (resetflag > 0) // uses a loop to only send once.
	{

		switch (deviceState)
		{
		case DEVICE_STATE_INIT:
		{
#if (AT_SUPPORT)
			getDevParam();
#endif
			printDevParam();
			LoRaWAN.init(loraWanClass, loraWanRegion);
			
			deviceState = DEVICE_STATE_JOIN;

			break;
		}
		case DEVICE_STATE_JOIN:
		{
			LoRaWAN.join();
			break;
		}
		case DEVICE_STATE_SEND:
		{
			if (resetflag <= 0)
			{
				break;
			} // break before sending

			resetflag--;
			prepareTxFrame(input, size);
			LoRaWAN.send();
			deviceState = DEVICE_STATE_CYCLE;
			break;
		}
		case DEVICE_STATE_CYCLE:
		{
			// Schedule next packet transmission
			txDutyCycleTime = appTxDutyCycle + randr(0, APP_TX_DUTYCYCLE_RND);
			LoRaWAN.cycle(txDutyCycleTime);
			deviceState = DEVICE_STATE_SLEEP;
			break;
		}
		case DEVICE_STATE_SLEEP:
		{
			LoRaWAN.sleep();
			break;
		}
		default:
		{
			deviceState = DEVICE_STATE_INIT;
			break;
		}
		}
	}
}

void LoRaConnect::loraSend()
{
	switch (deviceState)
	{
	case DEVICE_STATE_INIT:
	{
#if (AT_SUPPORT)
		getDevParam();
#endif
		printDevParam();
		LoRaWAN.init(loraWanClass, loraWanRegion);
		deviceState = DEVICE_STATE_JOIN;
		break;
	}
	case DEVICE_STATE_JOIN:
	{
		LoRaWAN.join();
		break;
	}
	case DEVICE_STATE_SEND:
	{
		LoRaWAN.send();
		deviceState = DEVICE_STATE_CYCLE;
		break;
	}
	case DEVICE_STATE_CYCLE:
	{
		// Schedule next packet transmission
		txDutyCycleTime = appTxDutyCycle + randr(0, APP_TX_DUTYCYCLE_RND);
		LoRaWAN.cycle(txDutyCycleTime);
		deviceState = DEVICE_STATE_SLEEP;
		break;
	}
	case DEVICE_STATE_SLEEP:
	{
		LoRaWAN.sleep();
		break;
	}
	default:
	{
		deviceState = DEVICE_STATE_INIT;
		break;
	}
	}
}

LoRaConnect::~LoRaConnect()
{
}