/*--------------------------------------------------------------------
  This file is part of the LoraWAN tester.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Thomas Randwijk Email: tgf.randwijk@student.han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/
/**
 * @file LoRaWANTester.cpp
 * @author Vincent de Man (V.deMan@student.han.nl)
 * @authors Thomas Randwijk, Ruben Pomstra, Thijs van Elsacker
 * @brief Main class for the loraWAN tester
 * @version 3.2
 * @date 2022-04-03
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "LoRaWANTester.h"

LoRaWANTester::LoRaWANTester()
{
}

void LoRaWANTester::Init()
{
  UI.Init();
  antenne.init();
  GPS.init();

  pinMode(Button, INPUT);
}

void LoRaWANTester::Loop()
{
  switch (UI.RefreshScreen())
  {
  case E_ClickSendScreenIDLE: // IDLE
    GPS.GPSupdate();          // keep updating in idle state
    break;

  case E_ClickSendScreenGPS: // GPS
    noInterrupts();
    ScreenStatus = E_ClickSendScreenSENDING_MESSAGE;
    GPS.GPSrun(1000); // get some time to update
    break;

  case E_ClickSendScreenSENDING_MESSAGE: // SEND
    ScreenStatus = E_ClickSendScreenSUCCESFULL;
    sendGPS();
    break;

  case E_ClickSendScreenSUCCESFULL: // SUCCES
    delay(200);
    interrupts();
    ScreenStatus = E_ClickSendScreenIDLE;
    break;

  case E_ClickSendScreenFAILED: // FAIL
    delay(200);
    interrupts();
    ScreenStatus = E_ClickSendScreenIDLE;
    break;

  case E_DistanceIntervalScreen:
    interrupts();
    delay(100);
    disableUpDownEnter();
    while (ScreenStatus == E_DistanceIntervalScreen)
    {
      UI.RefreshScreen();
      display1.displayOn();
      GPS.GPSrun(5000);
      sendGPS();
      display1.displayOff();
      if(ScreenStatus != E_DistanceIntervalScreen)
      {break;}
      GPS.GPSrun(10000);
    }
    enableUpDownEnter();
    break;
  }
}

void LoRaWANTester::disableUpDownEnter()
{
    detachInterrupt(digitalPinToInterrupt(buttonPinUp));
    detachInterrupt(digitalPinToInterrupt(buttonPinDown));
    detachInterrupt(digitalPinToInterrupt(buttonPinEnter));
}
void LoRaWANTester::enableUpDownEnter()
{
    attachInterrupt(digitalPinToInterrupt(buttonPinUp), interruptHandlerUp, FALLING);
    attachInterrupt(digitalPinToInterrupt(buttonPinDown), interruptHandlerDown, FALLING);
    attachInterrupt(digitalPinToInterrupt(buttonPinEnter), interruptHandlerEnter, FALLING);
}
void LoRaWANTester::sendGPS()
{
  GPS.GPSrun(500);
  uint8_t arr[8] = {0};
  if (((GPS.getLat() != 0.0) && (GPS.getLng() != 0.0)))
  {
    Serial.println("valid");

    encodeMessage(GPS.getLat(), GPS.getLng(), GPS.getAlt(), GPS.getHDOP(), arr);

    antenne.loraSend(arr, 8);
  }
  else
  {
    Serial.println("Invalid gps data");

    ScreenStatus = E_ClickSendScreenFAILED;
    UI.RefreshScreen();
  }
}

void LoRaWANTester::encodeMessage(const double lat, const double lng, const double alt, const double hdop, uint8_t (&arr)[8])
{
  double tus = (lat - 50.000) * 10000.0;
  int test = static_cast<int>(tus);
  arr[0] = test >> 8;
  arr[1] = test;

  double tss = (lng - 2.600) * 10000.0;
  int tess = static_cast<int>(tss);
  arr[2] = tess >> 8;
  arr[3] = tess;

  double ttt = (alt)*100.0;
  int tttt = static_cast<int>(ttt);
  arr[4] = tttt >> 8;
  arr[5] = tttt;

  double ab = hdop * 10;
  int abc = static_cast<int>(ab);
  arr[6] = abc;
}
