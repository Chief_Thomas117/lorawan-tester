[![pipeline status](https://gitlab.com/Chief_Thomas117/lorawan-tester/badges/main/pipeline.svg)](https://gitlab.com/Chief_Thomas117/lorawan-tester/-/commits/main)

# LoRaWAN Tester
The LoRaWAN Tester is an open source device that enables users to test LoRaWAN networks in their own area. 

## Features
The LoRaWAN Tester has multiple features
- A user interface: Options are displayed on an LCD and buttons are used to navigate through the interface.
- Settings: The LoRaWAN tester is adjustable via the interface: Send rate and display brightness can be changed.

## Hardware
The hardware used in this project:
- htcc-ab02s from Heltec
- Spring antenna (IPEX-1) from Heltec
- Tall buttons, resistors and a blank pcb with holes
- A self-made plastic case

## Software 
This project is built with PlatformIO in Visual Studio Code. The software used in this project are mainly libraries from HELTEC, written in C++/Arduino.

## Want to make this project yourself?
Create the physical device based on the diagrams.
Install Visual Studio Code, and within there install the following extensions: 
- PlatformIO IDE, GitLens, Doxygen Documentation Generator, C/C++

Clone the code from this project via Gitlens.

Compile and run the code on the htcc-ab02s.

Enjoy your LoRaWAN Tester!

## Support
For support you can send the project owner an e-mail.

## Contributing
This project is open to contributions.

## Authors and acknowledgment
This project is done by students from HAN University of Applied Sciences.

## License
This is an open source project.
